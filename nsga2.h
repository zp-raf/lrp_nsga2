#pragma once

#include "log.h"
#include "lrp.h"
#include "mobjective.h"
#include <functional>
#include <memory>
#include <vector>

namespace nsga2
{
	typedef std::vector<unsigned int> Chromosome;
	typedef std::vector<unsigned int> Route;
	typedef std::vector<Route> Routes;
	typedef std::vector<Routes> E1Routes;

	struct Individual : mobjective::BaseSolution
	{
		Individual() : mobjective::BaseSolution() {};
		Individual(unsigned int);
		Chromosome depots;
		Chromosome vehicles;
		Chromosome customers;

		// Stores first-echelon routes
		E1Routes e1routes;

		// NSGA-II attributes
		unsigned int rank;
		double crowdingDistance;

		// Used for fast non-dominated sorting procedure
		unsigned int dominationCount;
		std::vector<Individual*> dominatedIndividuals;

		// How much the solution violates all constraints
		double overallConstViolation;
	};

	typedef std::vector<Individual> Population;
	typedef std::vector<Population> NonDominatedFronts;
	//typedef std::vector<std::vector<double>> SolutionSet;

	// Compares two individuals using rank and crowded disntace
	bool crowdedComparisonOperator(Individual & lhs, Individual & rhs);

	// Determines wether a solution dominates another in a contrained optimization environment
	bool constDominates(Individual subject, Individual test);
	//double getError(Individual & ind, SolutionSet & ref);
	//unsigned int withinToleranceCount(SolutionSet & ref, Population & pop, double tolerance);
	
	class Solver
	{
	public:
		Solver(lrp::Problem & prob, unsigned int mPopSz, unsigned int mGen, double mut, double cross, unsigned int k);
		void findOptimum(/*SolutionSet referenceSet, double tolerance, */unsigned int maxTime, unsigned int logInterval_gen = 0, unsigned int logInterval_sec = 0);		
	private:
		lrp::Problem & problem;
		unsigned int maxPopSize;
		unsigned int maxGenerations;
		double pMut;
		double pCross;
		//unsigned int k;
		unsigned int paramK;

		// Stores the number of digits of the largest number among depot, vehicle and customer sets
		unsigned int maxDigits;

		// Converts from a NSGA2 individual to an LRP solution
		lrp::Solution convertToLRP(Individual);

		// Calculates the crowding distance in a given front
		void crowdingDistanceAssignment(Population &);

		// Creates a new individual based on the given ones
		Individual doCrossover(Individual, Individual);

		// Assigns objective function values and feasibility to population members
		void evalPop(Population &);

		// Produces a set of non-dominated fronts from a given population
		NonDominatedFronts fastNonDominatedSorting(Population);

		// Finds a solution for the first-echelon of an individual, using an exhaustive-search based heuristic
		void findFirstEchelonSolution(Individual &);

		// Assigns each population member a rank equal to its non-domination level (1 is the best, next is 2, ect.), and sorts the population by that rank
		void initPopNonDominatedSorting(Population &);

		// Does selection, crossover and mutation over a given population and produces a new offspring population
		Population makeNewPop(Population, unsigned int k);

		// Performs mutation on an individual
		void mutate(Individual &, unsigned int k);

		std::string popFirstFront2Plain(Population &);
		std::string pop2Plain(Population &);
		std::string pop2Python(Population &);

		void printInd(Individual);
		void printFronts(NonDominatedFronts);
		void printPop(Population, unsigned int n = 0);

		// Produces a random population of a given size
		Population randomPop(unsigned int size);

		// Repairs the genome of an individual after crossover
		void repair(Individual &);

		// Reorders an individual's chromosomes so that the depot's chromosome is in ascending order
		void reorderChromsomes(Individual &);

		// Select one individual after a binary tournament
		Individual selectIndividual(Population);		
	};
}