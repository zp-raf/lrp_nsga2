#include "lrp.h"
#include "nsga2.h"
#include <boost\property_tree\ptree.hpp>
#include <boost\property_tree\json_parser.hpp>
#include <exception>
#include <iostream>

#define OPT_NSGA2 "nsga2"
#define OPT_NSGA2_POP_SZ "maxPopSize"
#define OPT_NSGA2_MAX_GEN "maxGenerations"
#define OPT_NSGA2_P_MUT "mutProbability"
#define OPT_NSGA2_P_CROSS "crossoverProbability"
#define OPT_NSGA2_K "mutationParamK"
//#define OPT_NSGA2_REF "referenceSet"
//#define OPT_NSGA2_TOL "tolerance"
#define OPT_NSGA2_TIME "maxTime"
#define OPT_NSGA2_LOG_GEN "genIntervalLog"
#define OPT_NSGA2_LOG_TIME "timeIntervalLog"

#define SEPARATOR ' '

void printHelp()
{
	std::cerr << "Usage: " << std::endl;
	std::cerr << "\t problemDataFile solutionFile|{" << OPT_NSGA2
		<< " " << OPT_NSGA2_POP_SZ << " " << OPT_NSGA2_MAX_GEN << " " << OPT_NSGA2_P_MUT << " " << OPT_NSGA2_P_CROSS << " " << OPT_NSGA2_K
		<< OPT_NSGA2_TIME << " " << OPT_NSGA2_LOG_GEN << " " << OPT_NSGA2_LOG_TIME
	/*<< " [" << OPT_NSGA2_REF << " " << OPT_NSGA2_TOL << "]}"*/ << std::endl;
	std::cerr << "Explanation: " << std::endl;
	std::cerr << "\tproblemDataFile \t Path to a JSON file containing the problem's data" << std::endl;
	std::cerr << "\tsolutionFile \t Path to a JSON file containing a soution to the problem to be evaluated" << std::endl;
	std::cerr << "\t" << OPT_NSGA2 << "\t Solve the problem using a NSGA2 based MOEA" << std::endl;
	std::cerr << "\t" << OPT_NSGA2_POP_SZ << "\t Maximum population size parameter for the  MOEA" << std::endl;
	std::cerr << "\t" << OPT_NSGA2_MAX_GEN << "\t Maximum number of generations for the MOEA. Zero means no limit" << std::endl;
	std::cerr << "\t" << OPT_NSGA2_P_MUT << "\t Mutation probability of an individual of the MOEA" << std::endl;
	std::cerr << "\t" << OPT_NSGA2_P_CROSS << "\t Crossover probability of two individual of the MOEA" << std::endl;
	std::cerr << "\t" << OPT_NSGA2_K << "\t Adaptive mutation parameter of the MOEA, must be a positive integer" << std::endl;
	std::cerr << "\t" << OPT_NSGA2_TIME << "\t Maximum running time. Defaults to infinite" << std::endl;
	std::cerr << "\t" << OPT_NSGA2_LOG_GEN << "\t Generation interval between logging entries. Zero means log after every generation" << std::endl;
	std::cerr << "\t" << OPT_NSGA2_LOG_TIME << "\t Time interval between logging entries. Zero means log after every generation" << std::endl;
	//std::cerr << "\t" << OPT_NSGA2_REF << "\t File containing a reference pareto set to evaluate error against" << std::endl;
	//std::cerr << "\t" << OPT_NSGA2_TOL << "\t Tolerance [0, 1) to the reference pareto set, to be used as stop criterion" << std::endl;
}


//nsga2::SolutionSet getSolutions(std::string file)
//{
//	std::ifstream myCSV;
//	myCSV.open(file);
//	if (!myCSV)
//	{
//		myCSV.close();
//		throw std::exception("Reference set file does not exist");
//	}
//
//	nsga2::SolutionSet set;
//	std::string line;
//	while (std::getline(myCSV, line))
//	{
//		std::vector<double> tmp;
//		std::istringstream l(line);
//		std::string field;
//		bool go = true;
//		while (std::getline(l, field, SEPARATOR))
//		{
//			try
//			{
//				tmp.push_back(stod(field));
//			}
//			catch (const std::exception&)
//			{
//				go = false;
//			}
//		}
//		if (go)
//			set.push_back(tmp);
//	}
//	myCSV.close();
//	return set;
//}

int main(int argc, char* argv[])
{
	try
	{
		std::vector<std::string> args(argv, argv + argc); // load arguments into a vector of strings	

		// total number of arguments: exe+data+mode+pop+gen+mut+cross+k+time+int_get+int_time
		if (argc == 11 && args[2] == OPT_NSGA2) // nsga2
		{
			// the problem instance
			lrp::Problem instance(args[1]);
			nsga2::Solver solver(instance, std::stoul(args[3]), std::stoul(args[4]), std::stod(args[5]), std::stod(args[6]), std::stoul(args[7]));
			solver.findOptimum(/*{}, 0, */std::stoul(args[8]), std::stoul(args[9]), std::stoul(args[10]));
		}
		//// total number of arguments: exe+data+mode+pop+gen+mut+cross+k+time+int_get+int_time+ref+tol
		//else if (argc == 13 && args[2] == OPT_NSGA2 && std::stod(args[12]) < 1 && std::stod(args[12]) >= 0)
		//{
		//	// the problem instance
		//	lrp::Problem instance(args[1]);
		//	nsga2::Solver solver(instance, std::stoul(args[3]), std::stoul(args[4]), std::stod(args[5]), std::stod(args[6]), std::stoul(args[7]));
		//	solver.findOptimum(getSolutions(std::string(args[11])), std::stod(args[12]), std::stoul(args[8]), std::stoul(args[9]), std::stoul(args[10]));
		//}
		else
		{
			std::cerr << "Invalid arguments" << std::endl;
			printHelp();
		}
	}
	catch (std::exception &e)
	{
		std::cout << e.what();
	}
	return 0;
}