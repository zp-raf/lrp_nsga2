#include "nsga2.h"
#include <algorithm>
#include <chrono>
#include <ctime>
#include <functional>
#include <iterator>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <random>
#include <sstream>
#include <stack>
#include <utility>

using namespace nsga2;
using mobjective::dominates;

nsga2::Solver::Solver(lrp::Problem & prob, unsigned int mPopSz, unsigned int mGen, double mut, double cross, unsigned int k) :
	problem(prob), maxPopSize(mPopSz), maxGenerations(mGen), pMut(mut), pCross(cross), paramK(k)
{
	if (mut < 0 || mut > 1)
		throw "Mutation probability must be in range [0, 1]";
	else if (cross < 0 || cross > 1)
		throw "Crossover probability must be in range [0, 1]";

	// Establish maxDigits value
	auto max = std::max({ problem.getNumFacilities(), problem.getNumSources(), problem.getE2NumVehicles() });
	this->maxDigits = std::to_string(max).size();
}

unsigned int feasibleSolutions;
unsigned int feasibleSolutionsRun;

void nsga2::Solver::findOptimum(/*SolutionSet referenceSet, double tolerance, */unsigned int maxTime, unsigned int logInterval_gen, unsigned int logInterval_sec)
{
	/*
		VALIDATE PARAMETERS
	*/

	//if (tolerance < 0 || tolerance >= 1)
	//	throw std::exception("Invalid tolerance value. Must be [0, 1)");
	if (/*referenceSet.empty() &&*/ maxTime == 0 && maxGenerations == 0)
		throw std::exception("Invalid parameters: will result in an infinite running time!");
	if (maxTime > 0 && logInterval_sec > maxTime)
		throw std::exception("Invalid logging time interval");
	if (logInterval_gen > maxGenerations)
		throw std::exception("Invalid logging generations interval");

	/*
		INITIALIZE ALGORITHM VARIABLES
		(declared here to allow lambdas to access them)
	*/

	feasibleSolutions = 0;
	unsigned int it = 0;
	unsigned int lastLogIt = it;

	// time variables
	bool timerSet = false;
	std::time_t startTime, lastLogTime;
	std::chrono::steady_clock::time_point t0 = std::chrono::steady_clock::now(); // for calculating elapsed time in miliseconds

	Population parentPop;
	Population offspringPop;
	Population combinedPop;
	NonDominatedFronts fronts;

	//initialize adaptive mutation
	unsigned int k = paramK;
	unsigned int kCounter;
	if (maxGenerations > 0)
		kCounter = maxGenerations / paramK;
	else if (maxTime > 0)
		kCounter = maxTime / paramK;

	/*
		SETUP LOGGING
	*/

	Log log(problem.getName().c_str()); // the log file
	std::ofstream solutions; // another file for writing solutions
	solutions.open(std::string(log.getName()).append(".out"));
	std::stringstream out; // an output stream to buffer messages to the log

	/*
		DECLARE LAMBDA FUNCTIONS THAT DEFINE THE ALGORITHM'S BEHAVIOR
	*/

	// Evaluates if stopping criteria are met
	auto stopCondition = [&, maxIt = maxGenerations/*, err = getError*/]() {
		bool itCondition, /*tolCondition, */timeCondition;
		itCondition = maxIt > 0 ? it >= maxIt : false;
		//tolCondition = referenceSet.empty() ? false : true;
		//for (auto &p : offspringPop)
		//{
		//	if (referenceSet.empty())
		//		break;
		//	if (p.feasible && err(p, referenceSet) > tolerance)
		//	{
		//		tolCondition = false;
		//		break;
		//	}
		//}
		timeCondition = maxTime > 0 ? std::difftime(std::time(NULL), startTime) > maxTime : false;
		return itCondition || /*tolCondition ||*/ timeCondition;
	};

	// Produces output after each generation
	auto iterEndLog = [&, maxIt = maxGenerations, printer = std::bind(&Solver::pop2Plain, this, std::placeholders::_1)]() {
		if (logInterval_sec > 0 && std::difftime(std::time(NULL), lastLogTime) > logInterval_sec
			|| logInterval_gen > 0 && it - lastLogIt > logInterval_gen
			|| logInterval_gen == 0 && logInterval_sec == 0)
		{
			out.str("");
			out << "Iteration: " << it + 1
				<< ". Feasible solutions found during iteration: " << feasibleSolutionsRun
				<< ". Feasible solutions found so far: " << feasibleSolutions;
			log.message(out.str());

			if (it > 0) // dont print the first non dominated front on the first iteration
				solutions << printer(fronts.at(0)) << "\n"; // print first non-dominated front

			if (logInterval_sec > 0)
				lastLogTime = std::time(&lastLogTime);
			if (logInterval_gen > 0)
				lastLogIt = it;
		}
	};

	// Log final results
	auto presentResults = [&]() {
		// Calculate elapsed time in miliseconds
		std::chrono::steady_clock::time_point tf = std::chrono::steady_clock::now();
		std::chrono::duration<double> runTime = std::chrono::duration_cast<std::chrono::duration<double>>(tf - t0);
		out.str("");
		out << "Elapsed time: " << runTime.count() << " seconds";
		log.message(out.str());

		// Log final population
		out.str("");
		out << "Final population after " << it << " generations";
		//if (!referenceSet.empty())
		//	out << "MaxError: Maximum error of each solution with respect to the closest point in the reference set";
		out << "\n";
		unsigned int i = 1;
		bool printTitle = true;
		for (Individual ind : offspringPop)
		{
			if (printTitle)
			{
				out << "Individual Feasible";
				for (auto obj : ind.objectives)
				{
					out << " " << obj.name;
				}
				//if (!referenceSet.empty())
				//	out << " MaxError";
				out << std::endl;
				printTitle = false;
			}

			out << i++ << " " << (ind.feasible ? "Y" : "N") << std::setprecision(sizeof(double));
			for (unsigned int m = 0; m < problem.getNumObjectives(); ++m)
			{
				out << " " << ind.objectives.at(m).value;
			}
			//if (!referenceSet.empty())
			//	out << " " << getError(ind, referenceSet);
			out << std::endl;
		}
		log.message(out.str());

		//if (!referenceSet.empty())
		//{
		//	out.str("");
		//	out << "Reference set: \n";
		//	out << "Solution ";
		//	for (auto o : offspringPop.front().objectives)
		//	{
		//		out << " " << o.name;
		//	}
		//	out << std::endl;

		//	i = 1;
		//	for (auto l : referenceSet)
		//	{
		//		out << i++ << std::setprecision(sizeof(double));
		//		for (auto s : l)
		//		{
		//			out << " " << s;
		//		}
		//		out << std::endl;
		//	}
		//	log.message(out.str());
		//}
	};

	// Correctly updates the adaptive mutation parameter K
	auto updateParam = [&](unsigned int & k) {
		if (k > 0 && --kCounter == 0)
		{
			--k;
			if (maxGenerations > 0)
				kCounter = maxGenerations / paramK;
			else if (maxTime > 0)
				kCounter = maxTime / paramK;
		}
	};

	// Initial message
	out.str("");
	out << "MOEA started. Problem instance: " << problem.getName()
		<< ". Max population size: " << maxPopSize
		<< ". Max generations: " << maxGenerations
		<< ". Mutation probabillity: " << pMut
		<< ". Crossover probabillity: " << pCross
		<< ". K parameter: " << paramK;
	//if (!referenceSet.empty())
	//	out << ". Maximum error tolerance: " << tolerance;
	log.message(out.str());

	/*
		*************** NSGA2 START ***************
	*/

	// Create initial population
	parentPop = randomPop(maxPopSize);

	// Operate on the new population
	evalPop(parentPop);
	initPopNonDominatedSorting(parentPop);
	offspringPop = makeNewPop(parentPop, k);
	evalPop(offspringPop);

	// Main loop
	do
	{
		// Start timer 
		if (!timerSet && maxTime > 0)
		{
			std::time(&startTime);
			std::time(&lastLogTime);
			timerSet = true;
		}

		// Combine parent and offspring population
		combinedPop.clear();
		combinedPop.assign(parentPop.begin(), parentPop.end());
		combinedPop.insert(combinedPop.end(), offspringPop.begin(), offspringPop.end());
		fronts.clear();
		fronts = fastNonDominatedSorting(combinedPop); // get all non-dominated fronts from combined population

		/* proceed until the i-th front has more elements than it can be accomodated, that is,
		|parentPop|+|nonDominatedFront[i]| > maxPopSize */
		parentPop.clear();
		unsigned int i;
		for (i = 0; i < fronts.size(); i++)
		{
			crowdingDistanceAssignment(fronts.at(i));
			if (parentPop.size() + fronts.at(i).size() > maxPopSize)
				break;
			else
				parentPop.insert(parentPop.end(), fronts.at(i).begin(), fronts.at(i).end());
		}

		if (parentPop.size() < maxPopSize) // sorting the last front is only needed if the population is not full already
		{
			// sort in descending order using the crowded-comparison operator
			std::sort(fronts.at(i).begin(), fronts.at(i).end(), crowdedComparisonOperator);

			// put the first maxPopSize-|parentoPop| elements of fronts[i] into parentPop
			parentPop.insert(parentPop.end(), fronts.at(i).begin(), fronts.at(i).begin() + (maxPopSize - parentPop.size()));
		}

		// apply selection, crossover and mutation to parentPop to create a new offspring
		offspringPop = makeNewPop(parentPop, k);
		evalPop(offspringPop);

		// Iteration end message
		iterEndLog();

		// Increment generation counter and upated adative mutation parameter K
		++it;
		updateParam(k);
	} while (!stopCondition());

	presentResults(); // final log entries

	/*
		Print first-nondominated front
	*/

	//Population finalPop;
	//if (fronts.at(0).size() > maxPopSize)
	//{
	//	crowdingDistanceAssignment(fronts.at(0));
	//	// sort in descending order using the crowded-comparison operator
	//	std::sort(fronts.at(0).begin(), fronts.at(0).end(), crowdedComparisonOperator);
	//	finalPop.assign(fronts.at(0).begin(), fronts.at(0).begin() + maxPopSize - 1);
	//}
	//else
	//{
	//	finalPop.assign(fronts.at(0).begin(), fronts.at(0).end());
	//}
	//solutions << pop2Plain(finalPop) << "\n"; // print first non-dominated front
	solutions << pop2Plain(fronts.at(0)) << "\n"; // print first non-dominated front

	// Cleanup
	solutions.close();
}

template <class T>
std::vector<T> filter(std::vector<T> v) {
	std::vector<T> result;
	for (unsigned int i = 0; i < v.size(); i++)
		if (v[i] > 0) result.push_back(v[i]);
	return result;
}

// Generates a vector containing all partitions of a number n
std::vector<std::vector<unsigned int>> getPartitions(unsigned int n) {
	std::vector<std::vector<unsigned int>> result;
	std::vector<unsigned int> p(n); // An array to store a partition
	int k = 0; // Index of last element in a partition
	p[k] = n; // Initialize first partition as number itself

			  /*
			  This loop first prints current partition, then generates next
			  partition. The loop stops when the current partition has all 1s
			  */
	while (k >= 0)
	{
		// store current partition
		result.push_back(filter(p));

		// Generate next partition

		/*
		Find the rightmost non-one value in p[]. Also, update the
		rem_val so that we know how much value can be accommodated
		*/
		unsigned int rem_val = 0;
		while (k >= 0 && p[k] == 1)
		{
			rem_val += p[k];
			k--;
		}

		// if k < 0, all the values are 1 so there are no more partitions
		if (k < 0)
			break;

		// Decrease the p[k] found above and adjust the rem_val
		p[k]--;
		rem_val++;

		/*
		If rem_val is more, then the sorted order is violated.Divide
		rem_val in different values of size p[k] and copy these values at
		different positions after p[k]
		*/
		while (rem_val > p[k])
		{
			p[k + 1] = p[k];
			rem_val = rem_val - p[k];
			k++;
		}

		// Copy rem_val to next position and increment position
		p[k + 1] = rem_val;
		k++;
	}
	return result;
}

void nsga2::Solver::findFirstEchelonSolution(Individual & ind)
{
	// create a temporary facility customer assignment matrix for calculating supplied demand of a depot
	lrp::Solution::FacilityCustomerMatrix facilityCustomer(problem.getNumFacilities(), problem.getNumCustomers(), 0);
	for (unsigned int i = 0; i < ind.depots.size(); i++)
	{
		facilityCustomer.at_element(ind.depots.at(i), ind.customers.at(i)) = 1;
	}

	// Generate routes for each manufacturer
	ind.e1routes.clear();
	ind.e1routes.resize(problem.getNumSources());
	for (unsigned int l = 0; l < problem.getNumSources(); ++l)
	{
		std::vector<unsigned int> tourNodes; // stores customers that need only a single trip
		tourNodes.reserve(problem.getNumFacilities());
		for (unsigned int i = 0; i < problem.getNumFacilities(); ++i) // for each depot
		{
			if (std::find(ind.depots.begin(), ind.depots.end(), i) == ind.depots.end()) // if it is closed
				continue;

			unsigned int numTrips = problem.getNumTrips(l, i, facilityCustomer, problem);
			if (numTrips > 1)
				for (unsigned int r = 0; r < numTrips; ++r)
				{
					ind.e1routes.at(l).push_back({ l,i,l }); // insert a direct route to the depot
				}
			else if (numTrips > 0)
				tourNodes.push_back(i);
		}

		// If there are no nodes to visit in tours, proceed with the next manufacturer
		if (tourNodes.empty())
			continue;

		// Generate all possible route combinations, and find the one with the shortest overall length
		std::vector<std::vector<unsigned int>> partitions = getPartitions(tourNodes.size());
		Routes shortestSolution;
		double shortestOverallLength = INFINITY;
		unsigned int offset = problem.getNumSources();

		// each partition gives the number of routes and number of nodes to be visited
		for (auto partition : partitions)
		{
			// each permutation gives the order in which the nodes will be visited
			do
			{
				std::stack<unsigned int, std::vector<unsigned int>> availableNodes(tourNodes);
				Routes routeSet;
				double totalRouteLength = 0.0;
				bool isFeasibleSet = true;
				for (auto numNodes : partition)
				{
					Route route;
					unsigned int n = 0;

					route.push_back(l); // route starts at the manufacturer
					double routeDemand = 0.0;
					double routeLength = 0.0;
					double routeDuration = 0.0;
					while (n < numNodes && !availableNodes.empty())
					{
						route.push_back(availableNodes.top());
						routeDemand += problem.getFacilityDemand(l, availableNodes.top(), facilityCustomer);
						if (route.size() == 1)// if it has only the manufacturer
						{
							routeDuration += problem.getE1TravelTime(route.back(), availableNodes.top() + offset);
							routeLength += problem.getE1Distance(route.back(), availableNodes.top() + offset);
						}
						else // if it has other nodes in it
						{
							routeDuration += problem.getE1TravelTime(route.back() + offset, availableNodes.top() + offset);
							routeLength += problem.getE1Distance(route.back() + offset, availableNodes.top() + offset);
						}
						availableNodes.pop();
						n++;
					}
					routeLength += problem.getE1Distance(route.back() + offset, l);
					routeDuration += problem.getE1TravelTime(route.back() + offset, l);
					route.push_back(l); // route ends at the manufacturer
					if (routeDemand > problem.getE1VehicleCapacity()
						|| routeLength > problem.getE1VehicleMaxRteLength()
						|| routeDuration > problem.getE1VehicleMaxTravelTime())
					{
						isFeasibleSet = false;
						break;
					}
					else
					{
						routeSet.push_back(route);
						totalRouteLength += routeLength;
					}
				}
				// if the solution is the shortest, store it
				if (isFeasibleSet && totalRouteLength < shortestOverallLength)
				{
					shortestSolution = routeSet;
					shortestOverallLength = totalRouteLength;
				}
			} while (std::next_permutation(tourNodes.begin(), tourNodes.end()));
		}

		// store the shortest route in the individual
		for (auto route : shortestSolution)
		{
			ind.e1routes.at(l).push_back(route);
		}
	}
}

void nsga2::Solver::evalPop(Population & pop)
{
	feasibleSolutionsRun = 0;
	for (auto &ind : pop)
	{
		// Calculate first-echelon solution FIRST
		findFirstEchelonSolution(ind);
		//printInd(ind);
		lrp::Solution sol = convertToLRP(ind);
		//lrp::printSolution(sol, problem);
		ind.objectives.clear();
		ind.objectives = problem.getObjectiveValuesFrom(sol);

		// Check if more routes than permitted where generated
		unsigned int numRoutes = 0;
		for (auto m : ind.e1routes)
		{
			numRoutes += m.size();
		}

		if (numRoutes > problem.getE1MaxNumRoutes())
		{
			std::cerr << "WARNING: Found totally unfeasible solution" << std::endl;
			// make the solution absolutely bad
			ind.feasible = false;
			ind.overallConstViolation = INFINITY;
		}
		else
		{
			// proceed normally
			ind.overallConstViolation = problem.getOverallConstViolation(sol);
			//ind.feasible = !std::isgreater(ind.overallConstViolation, 0.0) && !std::isless(ind.overallConstViolation, 0.0);
			ind.feasible = problem.evalFeasibilityOf(sol);
		}

		if (ind.feasible)
		{
			feasibleSolutions++;
			feasibleSolutionsRun++;
		}

		//std::cout << "--------------------------------" << std::endl;
		//std::cout << "feasible: " << (ind.feasible ? "yes" : "no") << std::endl;
		//std::cout << "overall constrain violation: " << ind.overallConstViolation << std::endl;
	}
	//system("PAUSE");
}



Population nsga2::Solver::randomPop(unsigned int size)
{
	Population initPop;
	initPop.reserve(size);
	std::random_device engine;
	std::uniform_int_distribution<unsigned int> depotDist(0, problem.getNumFacilities() - 1);
	std::uniform_int_distribution<unsigned int> vehicleDist(0, problem.getE2NumVehicles() - 1);
	for (unsigned int i = 0; i < size; ++i)
	{
		Individual newIndividual(problem.getNumCustomers());

		// Fill vehicles and depots chromosome 
		std::map<unsigned int, unsigned int> vehicleDepot;
		for (unsigned int j = 0; j < problem.getNumCustomers(); ++j)
		{
			newIndividual.vehicles.at(j) = vehicleDist(engine);

			/* if the recently inserted vehicle index already has a depot associated,
			use that depot index instead of the randomly generated index */
			newIndividual.depots.at(j) = vehicleDepot.insert({ newIndividual.vehicles.at(j), depotDist(engine) }).first->second;
		}

		// Fill customers chromosome
		std::iota(newIndividual.customers.begin(), newIndividual.customers.end(), 0);
		std::shuffle(newIndividual.customers.begin(), newIndividual.customers.end(), engine);

		reorderChromsomes(newIndividual);
		initPop.push_back(newIndividual);
	}
	return initPop;
}

Population nsga2::Solver::makeNewPop(Population parentPop, unsigned int k)
{
	Population offspring;
	offspring.reserve(maxPopSize);

	// Select and do crossover
	bool newCouple = true;
	std::shared_ptr<Individual> parent1;
	std::shared_ptr<Individual> parent2;
	while (offspring.size() < maxPopSize)
	{
		if (newCouple)
		{
			parent1 = std::make_shared<Individual>(selectIndividual(parentPop));
			parent2 = std::make_shared<Individual>(selectIndividual(parentPop));
			newCouple = false;
			offspring.push_back(doCrossover(*parent1, *parent2));
		}
		else
		{
			offspring.push_back(doCrossover(*parent2, *parent1));
			newCouple = true;
		}
	}

	// Apply mutation
	for (auto &individual : offspring)
	{
		mutate(individual, k);
	}

	return offspring;
}

NonDominatedFronts nsga2::Solver::fastNonDominatedSorting(Population pop)
{
	NonDominatedFronts nonDominatedFronts(1); // size 1 to accomodate the first front

	for (Individual &p : pop)
	{
		p.dominatedIndividuals.clear();
		p.dominationCount = 0;
		for (Individual &q : pop)
		{
			if (constDominates(p, q)) // if P dominates Q
				p.dominatedIndividuals.push_back(&q); // add Q to the set of solutions dominated by P
			else if (constDominates(q, p)) // if Q dominates P
				p.dominationCount++; // increment domination counter of P
		}

		if (p.dominationCount == 0) // if P belongs to the first non-dominated front
		{
			p.rank = 0;
			nonDominatedFronts.front().push_back(p); // put P in the first front
		}
	}

	unsigned int i = 0; // initialize front counter
	while (!nonDominatedFronts.at(i).empty())
	{
		Population nextFront; // used to store members of the next front
		for (Individual &p : nonDominatedFronts.at(i))
		{
			for (Individual* q : p.dominatedIndividuals)
			{
				q->dominationCount--;
				if (q->dominationCount == 0) // Q belongs to the next front
				{
					q->rank = i + 1;
					nextFront.push_back(*q);
				}
			}
		}
		++i;
		nonDominatedFronts.push_back(nextFront);
	}
	return nonDominatedFronts;
}

void nsga2::Solver::initPopNonDominatedSorting(Population & pop)
{
	NonDominatedFronts fronts = fastNonDominatedSorting(pop);
	pop.clear();
	for (auto f : fronts)
	{
		for (auto i : f)
		{
			pop.push_back(i);
		}
	}
}

void nsga2::Solver::crowdingDistanceAssignment(Population & pop)
{
	for (Individual &i : pop) // initialize distances
	{
		i.crowdingDistance = 0;
	}
	for (unsigned int m = 0; m < problem.getNumObjectives(); m++) // for each objective function
	{
		// sort using objective values
		std::sort(pop.begin(), pop.end(), [m](Individual & lhs, Individual & rhs) -> bool {
			if (lhs.objectives.at(m).value < rhs.objectives.at(m).value)
				return true;
			else
				return false;
		});

		// both the first and last element have infinite crowding-distance, so that they are always selected
		pop.front().crowdingDistance = INFINITY;
		pop.back().crowdingDistance = INFINITY;

		// for all other points
		for (unsigned int i = 1; i < (pop.size() - 1); ++i)
		{
			if (pop.back().objectives.at(m).value - pop.front().objectives.at(m).value == 0) // in an ordered set if the max and min are equal then all intermediate points are equal too, so zero distance
				pop.at(i).crowdingDistance += 0;
			else
				pop.at(i).crowdingDistance += (pop.at(i + 1).objectives.at(m).value - pop.at(i - 1).objectives.at(m).value)
				/ (pop.back().objectives.at(m).value - pop.front().objectives.at(m).value);
		}
	}
}

nsga2::Individual::Individual(unsigned int numGenes)
{
	depots.resize(numGenes);
	vehicles.resize(numGenes);
	customers.resize(numGenes);
}

bool nsga2::constDominates(Individual subject, Individual test)
{
	if (subject.feasible && !test.feasible) // subject is feasible and test is not
		return true;
	// both solutions are infeasble but subject has a samller overall constraint violation
	else if (!subject.feasible && !test.feasible && subject.overallConstViolation < test.overallConstViolation)
		return true;
	//else if (!subject.feasible && !test.feasible && subject.overallConstViolation == test.overallConstViolation && mobjective::dominates(subject, test))
	//	return true;
	// both solutions are feasible and subject dominates test
	else if (subject.feasible && test.feasible && dominates(subject, test))
		return true;
	else
		return false;
}

bool nsga2::crowdedComparisonOperator(Individual & lhs, Individual & rhs)
{
	if (lhs.rank < rhs.rank)
		return true;
	else if (lhs.rank == rhs.rank && lhs.crowdingDistance > rhs.crowdingDistance)
		return true;
	else
		return false;
}
