#include "nsga2.h"
#include <algorithm>

#define FACILITY_NOT_ASSIGNED -1

lrp::Solution nsga2::Solver::convertToLRP(Individual source)
{
	lrp::Solution target(problem);

	/*
	SECOND-ECHELON SOLUTION DATA
	*/

	// First we deduce from the cromosome which facilities are open
	for (unsigned int i = 0; i < source.depots.size(); i++)
	{
		target.facilityStatus.at(source.depots.at(i)) = 1;
	}

	// Populate the facility-customer assignment matrix
	for (unsigned int i = 0; i < source.depots.size(); i++)
	{
		target.facilityCustomer.at_element(source.depots.at(i), source.customers.at(i)) = 1;
	}

	// Finally, convert the cromosome route data into a routing vector
	std::vector<bool> isFirstNode(problem.getE2NumVehicles(), true);
	std::vector<unsigned int> previousNode(problem.getE2NumVehicles());
	std::vector<int> vehicleFacility(problem.getE2NumVehicles(), FACILITY_NOT_ASSIGNED);
	unsigned int offset = problem.getNumFacilities();
	for (unsigned int i = 0; i < source.vehicles.size(); i++)
	{
		if (isFirstNode.at(source.vehicles.at(i)))
		{
			vehicleFacility.at(source.vehicles.at(i)) = source.depots.at(i);
			target.secondEchelonRoutes[source.depots.at(i)][source.customers.at(i) + offset][source.vehicles.at(i)] = 1;
			previousNode.at(source.vehicles.at(i)) = source.customers.at(i) + offset;
			isFirstNode.at(source.vehicles.at(i)) = false;
		}
		else
		{
			target.secondEchelonRoutes[previousNode.at(source.vehicles.at(i))][source.customers.at(i) + offset][source.vehicles.at(i)] = 1;
			previousNode.at(source.vehicles.at(i)) = source.customers.at(i) + offset;
		}
	}
	for (unsigned int i = 0; i < vehicleFacility.size(); ++i)
	{
		if (vehicleFacility.at(i) != FACILITY_NOT_ASSIGNED)
		{
			target.secondEchelonRoutes[previousNode.at(i)][vehicleFacility.at(i)][i] = 1;
		}
	}

	/*
	FIRST-ECHELON SOLUTION DATA
	*/

	unsigned int numSrc = problem.getNumSources();
	unsigned int numFac = problem.getNumFacilities();
	unsigned int maxE1routes = problem.getE1MaxNumRoutes();

	// Load first-echelon routes
	unsigned int r = 0;
	offset = numSrc;
	for (auto routes : source.e1routes)
	{
		for (auto route : routes)
		{
			if (route.empty())
				continue;
			if (r >= maxE1routes)
			{
				std::cerr << "WARNING: When loading E1routes: Exceeded maximum number of routes at the first echelon" << std::endl;
				goto srcFacLoading;
				//throw std::exception("Exceeded maximum number of routes at the first echelon");
			}
			for (unsigned int j = 1; j < route.size(); j++)
			{
				if (j == 1)
				{
					target.firstEchelonRoutes[route.at(j - 1)][route.at(j) + offset][r] = 1;
				}
				else if (j == route.size() - 1)
				{
					target.firstEchelonRoutes[route.at(j - 1) + offset][route.at(j)][r] = 1;
				}
				else
				{
					target.firstEchelonRoutes[route.at(j - 1) + offset][route.at(j) + offset][r] = 1;
				}
			}
			r++;
		}
	}

srcFacLoading:
	// Load source-facility assignment matrix
	for (auto routes : source.e1routes)
	{
		for (auto route : routes)
		{
			if (route.empty())
				continue;

			if (route.front() != route.back())
				throw std::exception("Non-closed tour at first-echelon");

			for (unsigned int node = 1; node < route.size() - 1; node++)
			{
				target.sourceFacility.at_element(route.front(), route.at(node)) = 1;
			}
		}
	}

	// Load transporter fraction of demand
	std::fill(target.fractionOfDemand.data(), target.fractionOfDemand.data() + target.fractionOfDemand.num_elements(), 0);

	typedef std::vector<unsigned int> Visitors;
	typedef std::vector<Visitors> DepotVisitors;
	typedef std::vector<DepotVisitors> ProductDepotVisitors;
	ProductDepotVisitors visits(problem.getNumSources(), DepotVisitors(problem.getNumFacilities()));

	r = 0; // vehicle index
	for (unsigned int l = 0; l < source.e1routes.size(); l++)
	{
		for (unsigned int route = 0; route < source.e1routes.at(l).size(); route++)
		{
			for (unsigned int node = 1; node < source.e1routes.at(l).at(route).size() - 1; node++)
			{
				visits.at(l).at(source.e1routes.at(l).at(route).at(node)).push_back(r);
			}
			r++;
		}
	}

	for (unsigned int l = 0; l < visits.size(); l++)
	{
		for (unsigned int i = 0; i < visits.at(l).size(); i++)
		{
			double numVisits = visits.at(l).at(i).size();
			double fractionDemand = static_cast<double>(numVisits == 0 ? 0 : 1 / numVisits);
			for (unsigned int v = 0; v < visits.at(l).at(i).size(); v++)
			{
				if (visits.at(l).at(i).at(v) >= maxE1routes)
				{
					std::cerr << "WARNING: When loading E1 Fraction Demand: Exceeded maximum number of routes at the first echelon" << std::endl;
					continue;
				}
				target.fractionOfDemand[l][i][visits.at(l).at(i).at(v)] = fractionDemand;
			}
		}
	}

	return target;
}

//double nsga2::getError(Individual & ind, SolutionSet & ref)
//{
//	double minError = INFINITY;
//	for (auto r : ref)
//	{
//		double maxError = 0;
//		for (unsigned int m = 0; m < ind.objectives.size(); m++)
//		{
//			double error = std::abs(ind.objectives[m].value - r[m]) / r[m];
//			maxError = error > maxError ? error : maxError;
//		}
//		minError = maxError < minError ? maxError : minError;
//	}
//	return minError;
//}
//
//unsigned int nsga2::withinToleranceCount(SolutionSet & ref, Population & pop, double tolerance)
//{
//	unsigned int count = 0;
//	for (auto p : pop)
//	{
//		if (getError(p, ref) <= tolerance)
//			count++;
//	}
//	return count;
//}

void nsga2::Solver::printInd(Individual ind)
{
	auto padded = [max = maxDigits](unsigned int val) {
		unsigned int digits = std::to_string(val).size();
		std::cout << val;
		for (unsigned int i = 0; i < max - digits + 1; i++)
			std::cout << " ";
	};

	std::cout << "----------------------------------" << std::endl;
	std::cout << "DEPOTS CHROMOSOME" << std::endl;
	for (auto c : ind.depots)
	{
		padded(c);
	}
	std::cout << std::endl << "VEHICLES CHROMOSOME" << std::endl;
	for (auto c : ind.vehicles)
	{
		padded(c);
	}
	std::cout << std::endl << "CUSTOMERS CHROMOSOME" << std::endl;
	for (auto c : ind.customers)
	{
		padded(c);
	}
	std::cout << std::endl << "----------------------------------" << std::endl;

	std::cout << "FIRST ECHELON ROUTES" << std::endl;
	for (auto m : ind.e1routes)
	{
		for (auto r : m)
		{
			std::cout << "["; padded(r.front()); std::cout << "]";
			for (unsigned int i = 1; i < r.size() - 1; i++)
			{
				std::cout << " -> "; padded(r.at(i));
			}
			std::cout << " -> ["; padded(r.back()); std::cout << "]" << std::endl;
		}
	}
}

void nsga2::Solver::printPop(Population pop, unsigned int n)
{
	for (auto ind : pop)
	{
		std::cout << n << ": ";
		for (auto obj : ind.objectives)
		{
			std::cout << obj.value << "; ";
		}
		std::cout << "feasible: " << (ind.feasible ? "yes" : "no")
			<< ", Violation: " << std::setprecision(sizeof(double)) << ind.overallConstViolation
			<< ", crowDist: " << ind.crowdingDistance
			//<< ", domCount: " << ind.dominationCount
			<< ", dominated: " << ind.dominatedIndividuals.size()
			<< ", rank: " << ind.rank;
		std::cout << std::endl;
		n++;
	}
}

std::string nsga2::Solver::popFirstFront2Plain(Population & pop)
{
	std::stringstream out;
	for (auto ind : pop)
	{
		if (ind.rank == 0)
		{
			out << std::setprecision(sizeof(double));
			for (unsigned int m = 0; m < problem.getNumObjectives(); ++m)
			{
				out << ind.objectives.at(m).value << (m == problem.getNumObjectives() - 1 ? "" : " ");
			}
			out << std::endl;
		}
	}
	return out.str();
}

std::string nsga2::Solver::pop2Plain(Population & pop)
{
	std::stringstream out;
	for (auto ind : pop)
	{
		out << std::setprecision(sizeof(double));
		for (unsigned int m = 0; m < problem.getNumObjectives(); ++m)
		{
			out << ind.objectives.at(m).value << (m == problem.getNumObjectives() - 1 ? "" : " ");
		}
		out << std::endl;
	}
	return out.str();
}

std::string nsga2::Solver::pop2Python(Population & pop)
{
	std::stringstream out;
	for (unsigned int ind = 0; ind < pop.size(); ++ind)
	{
		out << std::setprecision(sizeof(double));
		out << (ind == 0 ? "[ [ " : "[ ");
		for (unsigned int m = 0; m < problem.getNumObjectives(); ++m)
		{
			out << pop[ind].objectives[m].value << (m < problem.getNumObjectives() - 1 ? ", " : " ");
		}
		out << (ind == pop.size() - 1 ? "] ]" : "],") << std::endl;
	}
	out << std::endl;
	return out.str();
}

void nsga2::Solver::printFronts(NonDominatedFronts fronts)
{
	unsigned int f = 0;
	unsigned int n = 0;
	for (auto front : fronts)
	{
		std::cout << "------------------------" << f << "------------------------" << std::endl;
		printPop(front, n);
		n += front.size();
		f++;
	}
}