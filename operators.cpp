#include "nsga2.h"
#include <map>
#include <random>

#ifndef RIGHT
#define RIGHT 1
#endif // !RIGHT

#ifndef LEFT
#define LEFT 0
#endif // !LEFT

using namespace nsga2;

Individual nsga2::Solver::selectIndividual(Population pop)
{
	std::random_device engine;
	std::uniform_int_distribution<unsigned int> popDist(0, pop.size() - 1);

	unsigned int i, j;
	i = popDist(engine);
	j = popDist(engine);

	if (crowdedComparisonOperator(pop.at(i), pop.at(j)))
		return pop.at(i);
	else
		return pop.at(j);
}

Individual nsga2::Solver::doCrossover(Individual parent1, Individual parent2)
{
	//printInd(parent1);
	//printInd(parent2);
	// initialize random number generation
	std::random_device engine;
	std::uniform_real_distribution<double> crossoverDistr(0, 1);
	std::uniform_int_distribution<unsigned int> jcrossDistr(1, problem.getNumCustomers() - 1); // problem.getNumCustomers() gives us the number of genes of each chromosome

	Individual child(problem.getNumCustomers());
	if (crossoverDistr(engine) <= pCross)
	{
		unsigned int jcross = jcrossDistr(engine);
		for (unsigned int j = 0; j < jcross; j++)
		{
			// genes from the first parent
			child.depots.at(j) = parent1.depots.at(j);
			child.vehicles.at(j) = parent1.vehicles.at(j);
			child.customers.at(j) = parent1.customers.at(j);
		}
		for (unsigned int j = jcross; j < problem.getNumCustomers(); j++)
		{
			// genes from the second parent
			child.depots.at(j) = parent2.depots.at(j);
			child.vehicles.at(j) = parent2.vehicles.at(j);

			// find the first gene of the second parent's customer's chromosome that's not yet in the child's
			for (auto pGene : parent2.customers)
			{
				// find out if the gene is already on the child's chromosome
				bool found = false;
				unsigned int l = 0;
				while (!found && l < j)
				{
					if (child.customers.at(l) == pGene)
						found = true;
					++l;
				}

				// if it was not found in the child's customer's chromosome, assign the gene from the second parent
				if (!found)
				{
					child.customers.at(j) = pGene;
					break; // get out of the for loop, we don't need to continue searching
				}
			}
		}
		//printInd(child);
		// repair the genome of the new child
		repair(child);
	}
	else
	{
		// simply copy the first parent's genes into the child's
		child.depots.assign(parent1.depots.begin(), parent1.depots.end());
		child.vehicles.assign(parent1.vehicles.begin(), parent1.vehicles.end());
		child.customers.assign(parent1.customers.begin(), parent1.customers.end());
	}
	//printInd(child);
	reorderChromsomes(child);
	return child;
}

void nsga2::Solver::repair(Individual & ind)
{
	// stores, for each vehicle, the facility index assigned. If no facility is assigned, then its -1
	std::map<unsigned int, unsigned int> vehicleDepot;

	// preserve the very first assignment of facility to a vehicle through the entire cromosome
	for (unsigned int i = 0; i < ind.vehicles.size(); ++i)
	{
		// if a facility is not yet assigned to the present vehicle, assign the current one
		ind.depots.at(i) = vehicleDepot.insert({ ind.vehicles.at(i), ind.depots.at(i) }).first->second;
	}
}

void nsga2::Solver::mutate(Individual & ind, unsigned int k)
{
	std::random_device engine;
	std::uniform_real_distribution<double> mutDistr(0, 1);
	std::uniform_int_distribution<unsigned int> geneDistr(0, ind.customers.size() - 1); // gene index distribution
	std::function<double(unsigned int)> swapProbability = [](unsigned int k)->double {
		return (double)2 / ((k + 1)*(k + 2));
	};
	double mut = mutDistr(engine);

	// Adaptive customer chromosome mutation
	if (mut <= swapProbability(k))
	{
		for (unsigned int m = 0; m < k; ++m)
		{
			auto i = geneDistr(engine);
			auto j = geneDistr(engine);
			std::iter_swap(ind.customers.begin() + i, ind.customers.begin() + j);
		}
	}

	if (mut <= pMut)
	{
		/* Depot mutation: select a random position then replace all occurrences of that number in that position
		with a new random number */
		std::uniform_int_distribution<unsigned int> depotDistr(0, problem.getNumFacilities() - 1);
		unsigned int selectedPos = geneDistr(engine);
		unsigned int newVal = depotDistr(engine);
		std::replace(ind.depots.begin(), ind.depots.end(), ind.depots.at(selectedPos), newVal);

		/* Vehicle mutation: select a random position then replace all occurrences of that number in that position
		with a new random number */
		std::uniform_int_distribution<unsigned int> vehDistr(0, problem.getNumFacilities() - 1);
		selectedPos = geneDistr(engine);
		newVal = depotDistr(engine);
		std::replace(ind.vehicles.begin(), ind.vehicles.end(), ind.vehicles.at(selectedPos), newVal);
		repair(ind); // the individual should be repaired in case first vehicle mutation makes it unfeasible
		reorderChromsomes(ind); // must reorder chromosomes again in order to maintain validity

		// Given an initial position of a vector and a search direction, returns the first position of the next adjacent block. If not found, returns vector.size()
		std::function<unsigned int(Chromosome, unsigned int, unsigned int)> adjBlock =
			[](Chromosome vec, unsigned int startPos, unsigned int direction)->unsigned int {
			if (startPos > vec.size())
				throw "Second mutation of vehicles: Invalid starting position";
			unsigned int i = startPos;
			while (0 <= i && i < vec.size())
			{
				// if at the leftmost element the adjacent block is not found yet
				if (i == 0 && vec.at(i) == vec.at(startPos) && direction == LEFT)
					return vec.size();
				else if (vec.at(i) != vec.at(startPos)) // if a new block is found
					return i;
				else if (direction == RIGHT)
					++i;
				else
					--i;
			}
			return i; // if past the end of the vector, a new block is not found yet
		};

		/* Second vehicle mutation, increase number of clients served by a vehicle */
		std::uniform_int_distribution<unsigned int> directionDist(0, 1);
		unsigned int direction = directionDist(engine);
		selectedPos = geneDistr(engine);
		unsigned int nextBlock = adjBlock(ind.vehicles, selectedPos, direction);
		if (nextBlock < ind.vehicles.size()) // if an adjacent block was found
		{
			ind.depots.at(nextBlock) = ind.depots.at(selectedPos);
			ind.vehicles.at(nextBlock) = ind.vehicles.at(selectedPos);
		}
		else
		{
			// try searching in the opposite direction
			nextBlock = adjBlock(ind.vehicles, selectedPos, direction == 0 ? 1 : 0);
			if (nextBlock < ind.vehicles.size()) // if it was found
			{
				ind.depots.at(nextBlock) = ind.depots.at(selectedPos);
				ind.vehicles.at(nextBlock) = ind.vehicles.at(selectedPos);
			}
		}
	}
}

void nsga2::Solver::reorderChromsomes(Individual & ind)
{
	/*
	Copy individual's cromosomes into a temporary data structure to facilitate sorting
	*/

	struct Gene {
		int depot;
		int vehicle;
		int customer;
	};
	std::vector<Gene> chromosome(problem.getNumCustomers());
	for (unsigned int i = 0; i < chromosome.size(); ++i)
	{
		chromosome.at(i).depot = ind.depots.at(i);
		chromosome.at(i).vehicle = ind.vehicles.at(i);
		chromosome.at(i).customer = ind.customers.at(i);
	}

	// sort the temporary vector using a custom predicate
	std::sort(chromosome.begin(), chromosome.end(), [](const Gene &lhs, const Gene &rhs) {
		return lhs.depot < rhs.depot;
	});

	// replace back the individual's cromosome
	for (unsigned int i = 0; i < chromosome.size(); ++i)
	{
		ind.depots.at(i) = chromosome.at(i).depot;
		ind.vehicles.at(i) = chromosome.at(i).vehicle;
		ind.customers.at(i) = chromosome.at(i).customer;
	}
}