# About

This is an implementation of a NSGA-II based evolutionary algorithm, to solve a Two-Echelon Green Location-Routing Problem.

# Dependencies

* Visual Studio 2015 or newer.
* boost libraries 1.63.00 or newer <http://www.boost.org>
* lrp5 static library <https://bitbucket.org/zp-raf/lrp5>

# Setup

1. Create an environmet variable `VSPROJDIR` containing the full path to the directory in which the repo will be cloned
2. Clone the repo in `VSPROJDIR`
3. Clone [lrp5 static library](https://bitbucket.org/zp-raf/lrp5) into `VSPROJDIR` as well
4. Download in decompress boost 1.63.00 library in `C:\boost`
5. Open project in Visual Studio and check _Additional Include Directories_ for `C:\boost\boost_1_63_0` libraries and `%VSPROJDIR%\lrp5` project
6. Check dependecies within Visutal Studio: make sure `lrp5` project is checked.

# Usage

The application command line has the form:

	lrp_nsga2.exe problemDataFile solutionFile|{nsga2 maxPopSize maxGenerations mutProbability crossoverProbability mutationParamK maxTime genIntervalLog timeIntervalLog}
	
Where:

* `problemDataFile`: Path to a JSON file containing the problem's data.
* `solutionFile`: Path to a JSON file containing a solution to the problem to be evaluated. Calculates the objective function values for the provided solution.
* `nsga2`:	Solve the problem using a NSGA2 based MOEA.
* `maxPopSize`: Maximum population size parameter for the  MOEA.
* `maxGenerations`: Maximum number of generations for the MOEA. Zero means no limit.
* `mutProbability`: Mutation probability of an individual of the MOEA.
* `crossoverProbability`: Crossover probability of two individual of the MOEA.
* `mutationParamK`: Adaptive mutation parameter of the MOEA, must be a positive integer.
* `maxTime`: Maximum running time, in seconds. Defaults to infinite.
* `genIntervalLog`: Generation interval between log entries. Zero means log after every generation.
* `timeIntervalLog`: Time interval between log entries, in seconds. Zero means log after every generation.
	
For a given problem file named `problemData.json`, the program output will consist of:

1. A `problemData.log` file containing log entries, such as start time, elapsed time and other information.
2. A `problemData.out` file containing the Pareto fronts found during algorithm execution, separated by a blank line (note that the best front will always be the last).

The `genIntervalLog` and `timeIntervalLog` parameters also control the amount of output data. The Pareto fronts found during execution will be written to the output file at the same intervals as the log entries.

## Included test instances

Five problem instances, described in the next table, are provided the `test_instances` directory.

| Instance | # Manufacturers | # CDCs | # Customers|
|----|---|---|---|
| **XP.json** | 2 | 2 | 5 |
| **P.json** | 3 | 2 | 13 |
| **M.json** | 5 | 3 | 27 |
| **L.json** | 8 | 4 | 40 |
| **XL.json** | 10 | 5 | 53 |

## Computing the Hypervolume

Hypervolume of the found Pareto sets can be calculated using third-party software as [Computation of the Hypervolume Indicator by Fonseca et al.](http://lopez-ibanez.eu/hypervolume)